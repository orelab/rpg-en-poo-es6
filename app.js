//Ennemys
import { Assassin } from './class_ennemy/Assassin.js';
import { Berserker } from './class_ennemy/Berserker.js';
import { Dragon } from './class_ennemy/Dragon.js';
import { Ennemy } from './class_ennemy/ennemy.js';
import { Golem } from './class_ennemy/Golem.js';
import { Griffin } from './class_ennemy/Griffin.js';
import { Werewolf } from './class_ennemy/Werewolf.js';
//Heros
import { Hero } from './class_hero/Hero.js';
import { Dwarf } from './class_hero/Dwarf.js';
import { Elf } from './class_hero/Elf.js';
import { Human } from './class_hero/Human.js';

let golem1 = new Golem('Florian Golem');
console.log(golem1.type);

let human1 = new Human('Je suis l\'humain et je vais te manger');
console.log(human1);

let elf1 = new Elf('Je suis l\'Elf et je vais te chasser');
console.log(elf1);

let dragon1 = new Dragon('Shenron');
console.log(dragon1.getHitStrength());

let partyOver = false;


let vieAuDebut = 'Humain : ' + human1.getHealth() + ' Dragon : ' + dragon1.getHealth();




//Battle

console.log(vieAuDebut);

while (!partyOver) {
    //Attaque de l'humain
    human1.attack(dragon1);
    console.log(human1.name + ' : Attaque');
    console.log("Vie du Dragon : " + dragon1.getHealth())

    if (dragon1.die()) {

        console.log(dragon1.name + ' : Est mort')
        partyOver = true;
    } else {
        //Attaque du dragon
        dragon1.attack(human1);
        console.log(dragon1.name + ' : Attaque');
        console.log("Vie de l'Humain : " + human1.getHealth())

        if (human1.die()) {
            console.log("Vie de l'Humain : " + human1.getHealth())
            console.log(human1.name + ' : Est mort')
            partyOver = true;
        }
    }
    if (dragon1.die()) {
        console.log('Aragorn a tué Shenron')
    } else if (human1.die()) {
        console.log('Sheron a tué Aragorn')
    }
}


// Submit du formulaire "lanceCombat" index.html
function lanceCombat() {

    let combatantName1 = document.getElementById("name1").value;

    let combatantName2 = document.getElementById("name2").value;

    let combatantRace1 = document.getElementById("Fighter1").value;

    let combatantRace2 = document.getElementById("Fighter2").value;


    console.log(combatantName1 + ' le ' + combatantRace1 + ' vs. ' + combatantName2 + ' le ' + combatantRace2);

}