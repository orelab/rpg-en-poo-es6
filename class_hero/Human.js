import { Hero } from './Hero.js';

export class Human extends Hero {

    constructor(name, race) {

        super(name, race);

        this.name = 'Aragorn';

        this.race = 'Human';

        this.hitStrength *= 1.1;

        /* this.attackSky = hitStrength *= 0.9; */
    }



    attack(cible) {
        const newHealth = cible.getHealth() - this.hitStrength;
        cible.setHealth(newHealth);
    }

    // attack(cible) {
    //      if (ennemyVolant) {
    //          this.attackSky
    //      } else {
    //          this.hitStrength
    //      }
    //     let nouvelleVie = cible.getHealth() - this.hitStrength
    // }
}


// Humans : +10% de hitStrength sur les ennemis au sol, -10% de hitStrength sur les ennemis volants