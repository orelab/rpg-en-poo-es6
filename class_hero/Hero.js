export class Hero {

    name;
    race;
    health = 100;
    hitStrength = 30;
    lvl = 1;
    xp = 0;

    constructor(name, race) {

        this.name = name;
        this.race = race;

    }

    getName() {
        return this.name;
    }

    setName(newName) {
        this.name = newName
    }

    getHealth() {
        return this.health;
    }

    setHealth(health) {
        this.health = health;
    }

    attack(cible) {
        const newHealth = cible.getHealth() - this.hitStrength;
        cible.setHealth(newHealth);
        // return cible.health - this.hitStrength;
        // return this.hitStrength;
    }

    die() {
        if (this.health <= 0) {
            return true;
        } else {
            return false;
        }
    }

    getLvl() {
        return this.lvl;
    }

    setLvl(newLvl) {
        this.lvl = newLvl;
    }

    getXp() {
        return this.xp;
    }

    setXp(newXp) {
        this.xp = newXp;
    }

    getRace() {
        return this.race;
    }

    setRace(newRace) {
        this.race = newRace;
    }

    getHitStrength() {
        return this.hitStrength;
    }

    setHitStrength(newHitStrength) {
        this.HitStrength = newHitStrength;
    }

    levelUp() {
        if (this.xp == 10) {
            this.xp -= 10;
            this.lvl++;
            this.health += 10;
            this.hitStrength += 5;
        }
    }

}