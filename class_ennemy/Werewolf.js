import {Ennemy} from './ennemy.js';

export class Werewolf extends Ennemy{
    constructor(name){
        this.health *= 1.5;
    }
}

