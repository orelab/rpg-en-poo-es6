import {Ennemy} from './ennemy.js';

export class Dragon extends Ennemy{
    constructor(name, type){
        super(name,type);
        this.type = 'Dragon'
    }

    attackFromGround(){


    }

    attackFromSky(){

    }

    fly(){
    
    }

    resistance(){

    }
}