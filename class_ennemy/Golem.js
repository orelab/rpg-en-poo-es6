import {Ennemy} from './ennemy.js';

export class Golem extends Ennemy{
    constructor(name, type){
        super(name);
        this.type = 'Golem';
    }

    talk(){
        return console.log(this.name + " : je suis le Golem et je vais te goumer");
    }
}

