export class Ennemy {
    constructor(name, type){
        
         this.name = name;
         this.health = 90;
         this.type = type;
         this.hitStrength = 10;
         this.lvl = 1;
         this.xp = 0;
    }
    // Récupère le nom
    getName(){
        return this.name;
    }

    //Lui donne un nom
    setName(value){
        this.name = value;
    }

    // Récupère la vie
    getHealth(){
        return this.health;
    }

    // Modifie la valeur actuelle de la vie
    setHealth(health){
        this.health = health;
    }

    // Permet d'attaquer
    attack(adversaire){
        const newHealth = adversaire.getHealth() - this.hitStrength;
        adversaire.setHealth(newHealth);
    }

    // Le personnage meurt une fois que sa vie superieur ou egale a 0
    die(){
/*         if(this.health <= 0){
            return true;
        } else{
            return false;
        } */

        return this.health <= 0;
    }

    // récupère le niveau
    getLvl(){
        return this.lvl;
    }
    
    // Modifie la valeur actuelle du niveau
    setLvl(newLvl){
        this.lvl = newLvl;
    }

    //Récupère l'XP
    getXp(){
        return this.xp;
    }

    // attribue de nouveau XP
    setXp(newXp){
        this.xp = newXp;
    }

    // Récupère la puissance de frappe
    getHitStrength(){
        return this.hitStrength;
    }

    // Modifie la valeur actuelle de la puissance de frappe
    setHitStrength(newHitStrength){
        this.hitStrength = newHitStrength;
    }

    getType(){
        return this.type;
    }

    setType(newType){

    }

    // Si le niveau est égale a 10 alors on repart de 0 et on lui attribu un nouveau level et récupère 5 en vie et 5 en puissance de frappe
    levelUp(){
        if(this.xp == 10){
            this.xp -= 10;
            this.lvl++;
                this.health += (this.health + 5);
                this.hitStrength += (this.hitStrength + 5);
        }
    }
    
}



